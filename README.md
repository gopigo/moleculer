Sample script of how to use Moleculer: `index.js`

Also implemented in this [RunKit](https://runkit.com/therobotacademy/5d42cfaf57231300168e7b9a)

*NOTE: When needing a production transporter use NATS (located at `./examples/docker-compose.yaml`). TCP transporter is experimental***

# 1 Basic GET server-client implementation
## Folder `listProducts-demo`
https://moleculer.services/docs/0.13/concepts.html#Implementation

Required dependencies are in folder `./src`

`$ node index.js`

Visit: http://localhost:3000/products

`[{"name":"Apples","price":5},{"name":"Oranges","price":3},{"name":"Bananas","price":2}]`

This is how the GET request is handled:
```
aliases: {
          // When the "GET /products" request is made the "listProducts" action of "products" service is executed
          "GET /products": "products.listProducts"
        }
      }
```
# 2 Short examples

Required dependencies: `$ npm install moleculer-repl`

## (previous) REPL Console
Usage: 

```
$ node
let ServiceBroker = require("./src/service-broker");
const broker = new ServiceBroker();

// Switch to REPL mode
broker.repl();
```
Console usage https://moleculer.services/docs/0.13/moleculer-repl.html
```
mol $ nodes
mol $ nodes -d
mol $ services
mol $ actions
mol $ events
mol $ info
mol $ env
mol $ call math.add --a 1 --b 2 # See 2.2 Server & client nodes below
```

## Folder `examples`
From `examples` folder in official [moleculer repo](https://github.com/moleculerjs/moleculer)

### 2.1 Simple
https://moleculer.services/docs/0.13/examples.html#Simple

`$ npm run demo simple`

    - package.json script: "demo": "node examples/index.js simple"
        - `index.js` redirects to `simple` module folder

### 2.2 Server & client nodes
https://moleculer.services/docs/0.13/examples.html#Server-amp-client-nodes

- Server: `$ npm run demo client-server/server`
    - = `$ node examples/client-server/server.js`
- Client: `$ npm run demo client-server/client`
    - = `$ node examples/client-server/client.js`

Server and Client also launch Moleculer REPL:
```
mol $ call math.add --a 1 --b 2
mol $ call math.add '{"a":1, "b":2}'
mol $ call math.add '{"a":1, "b":2}' --save
mol $ call "math.add" --stream picture.png
mol $ dcall "server-3293" "$node.health"
mol $ emit "user.created" --a 5 --b Bob --c --no-d --e.f "hello"
mol $ bench math.add
mol $ bench --num 3 math.add
mol $ bench --time 1 math.add '{"a":1, "b":2}'
```

## 2.3 Middlewares

Dependencies: `npm i fakerator`

`$ npm run demo middlewares`

## 2.4 Runner
This example shows how you can start a broker and load services with Moleculer Runner (some dependencies missed; look at package.json of https://github.com/moleculerjs/moleculer):

`$ node ./bin/moleculer-runner.js -c examples/runner/moleculer.config.js -r examples/user.service.js`

## 2.5 Load tester
- Server: `$ node examples/loadtest/server.js`
- Client: `$ node examples/loadtest/clients.js`


# 3 Docker Compose prototype ('project' template)
## Folder `moleculer-demo`
https://moleculer.services/docs/0.13/usage.html

## How to generate the project prototype
Install the CLI: `$ sudo npm i moleculer-cli -g`

Then initilialize the project with the CLI: `$ moleculer init project moleculer-demo`

This places a `docker-compose` file in folder.

You need to have NATS installed, then you would better run Moleculer via Docker as it was generated by the CLI:

`$ docker-compose up -d`

Then:
- Visit `http://localhost:3002 or 3000 (default)` and check the resulting API. (calll port 3002 if you modify the template of the docker-compose template)
- Visit `http://localhost:3001` to access the Traefik dashboard.


If services were configure in the host, just do this:
```
$ cd moleculer-demo
$ npm i
$ npm start
```
