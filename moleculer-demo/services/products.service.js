"use strict";

const DbService = require("../../../Moleculer-api-rest-NATS/services/node_modules/moleculer-db");

module.exports = {
	name: "products",
	mixins: [DbService]
};